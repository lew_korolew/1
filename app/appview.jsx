var React = require("react");
var connect = require("react-redux").connect;
var actions = require("./actions.jsx");
 
class TownForm extends React.Component {
  constructor(props) {
    super(props);
  }
  onClick() {
    if (this.refs.townInput.value !== "") {
      var itemText = this.refs.townInput.value;
      this.refs.townInput.value ="";
      return this.props.addTown(itemText);
    }
  }
  render() {
    return <div>
            <input ref="townInput" />
            <button onClick = {this.onClick.bind(this)}>Добавить</button>
        </div>
  }
};
 
class TownItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
     
    return <div>
              {this.props.text}
              <button onClick={() => this.props.deleteTown(this.props.text)}>Удалить</button> 
            </div>
  }
};
 
class TownsList extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <div>
        {this.props.towns.map(item =>
          <TownItem text = {item}
                    deleteTown = {this.props.deleteTown}/>
        )}
      </div>
  }
};
 
class AppView extends React.Component {
 
    render() {
        return <div>
            <TownsList {...this.props} />
            <TownForm addTown={this.props.addTown}/>
    </div>
  }
};
 
function mapStateToProps(state) {
  return {
    towns: state
  };
}
 
module.exports = connect(mapStateToProps, actions)(AppView);