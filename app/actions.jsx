var addTown = function (town) {
  return {
    type: "ADD_TOWN",
    town
  }
};
var deleteTown = function (town) {
  return {
    type: "DELETE_TOWN",
    town
  }
};
 
module.exports = {addTown, deleteTown};