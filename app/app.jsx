var React = require("react");
var ReactDOM = require("react-dom");
var redux = require("redux");
var Provider = require("react-redux").Provider;
var AppView = require("./appview.jsx");

var reducer = function(state = [], action) {
  switch (action.type) {
    case "SET_STATE":
        return state.concat(action.towns);
    case "ADD_TOWN":
        return state.concat([action.town]);
    case "DELETE_TOWN":
        return state.filter(a => a !== action.town);
  }
  return state;
}

var store = redux.createStore(reducer);
 
store.dispatch({
  type: "SET_STATE",
  towns: [ "Архангельск", "Брянск" ]
});
 
 
ReactDOM.render(
  <Provider store={store}>
    <AppView />
  </Provider>,
  document.body
);